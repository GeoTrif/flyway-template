package com.flywaytemplate.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;

@Entity
@Table(name = "address_one_to_one")
public class AddressOneToOne {

    @Id
    @GeneratedValue
    @Column(name = "address_id")
    private int address_id;

    @Column(name = "streetName")
    private String streetName;

    @Column(name = "cityName")
    private String cityName;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    private CustomerOneToOne customerOneToOne;

    public AddressOneToOne() {
    }

    public AddressOneToOne(final int address_id, final String streetName, final String cityName, final CustomerOneToOne customerOneToOne) {
        this.address_id = address_id;
        this.streetName = streetName;
        this.cityName = cityName;
        this.customerOneToOne = customerOneToOne;
    }

    public int getAddress_id() {
        return address_id;
    }

    public void setAddress_id(int address_id) {
        this.address_id = address_id;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public CustomerOneToOne getCustomerOneToOne() {
        return customerOneToOne;
    }

    public void setCustomerOneToOne(CustomerOneToOne customerOneToOne) {
        this.customerOneToOne = customerOneToOne;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
