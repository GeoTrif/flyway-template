package com.flywaytemplate.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;

@Entity
@Table(name = "customer_one_to_one")
public class CustomerOneToOne {

    @Id
    @GeneratedValue
    @Column(name = "customer_id")
    private int customer_id;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lasName;

    @OneToOne(mappedBy = "customerOneToOne", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    private AddressOneToOne addressOneToOne;

    public CustomerOneToOne() {
    }

    public CustomerOneToOne(final int customer_id, final String firstName, final String lasName, final AddressOneToOne addressOneToOne) {
        this.customer_id = customer_id;
        this.firstName = firstName;
        this.lasName = lasName;
        this.addressOneToOne = addressOneToOne;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLasName() {
        return lasName;
    }

    public void setLasName(String lasName) {
        this.lasName = lasName;
    }

    public AddressOneToOne getAddressOneToOne() {
        return addressOneToOne;
    }

    public void setAddressOneToOne(AddressOneToOne addressOneToOne) {
        this.addressOneToOne = addressOneToOne;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
