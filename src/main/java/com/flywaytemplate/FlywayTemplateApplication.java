package com.flywaytemplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlywayTemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlywayTemplateApplication.class, args);
	}

}
