package com.flywaytemplate.controller;

import com.flywaytemplate.model.CustomerOneToOne;
import com.flywaytemplate.service.CustomerOneToOneService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/customer-test")
public class CustomerOneToOneController {

    private final CustomerOneToOneService customerOneToOneService;

    public CustomerOneToOneController(final CustomerOneToOneService customerOneToOneService) {
        this.customerOneToOneService = customerOneToOneService;
    }

    @GetMapping
    public void getAllCustomers() {
        List<CustomerOneToOne> customers = customerOneToOneService.getAllCustomers();
        customers.forEach(System.out::println);
    }
}
