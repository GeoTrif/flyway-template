package com.flywaytemplate.dao;

import com.flywaytemplate.model.AddressOneToOne;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressOneToOneDao extends JpaRepository<AddressOneToOne, Integer> {
}
