package com.flywaytemplate.dao;

import com.flywaytemplate.model.CustomerOneToOne;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerOneToOneDao extends JpaRepository<CustomerOneToOne, Integer> {
}
