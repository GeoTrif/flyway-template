package com.flywaytemplate.service;

import com.flywaytemplate.dao.CustomerOneToOneDao;
import com.flywaytemplate.model.CustomerOneToOne;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerOneToOneService {

    private final CustomerOneToOneDao customerOneToOneDao;

    public CustomerOneToOneService(final CustomerOneToOneDao customerOneToOneDao) {
        this.customerOneToOneDao = customerOneToOneDao;
    }

    public List<CustomerOneToOne> getAllCustomers() {
        return customerOneToOneDao.findAll();
    }
}
