CREATE TABLE `customer_one_to_one` (
    `customer_id` INT NOT NULL,
    `firstName` VARCHAR(45) NOT NULL,
    `lastName` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`customer_id`)
    ) ENGINE=InnoDB;

CREATE TABLE `address_one_to_one` (
    `address_id` INT NOT NULL,
    `streetName` VARCHAR(45) NOT NULL,
    `cityName` VARCHAR(45) NOT NULL,
    `customer_id` INT NOT NULL,
    PRIMARY KEY (`address_id`),
    CONSTRAINT `customer_fk_one_to_one` FOREIGN KEY (`customer_id`) REFERENCES `customer_one_to_one` (`customer_id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB;

CREATE TABLE `customer_one_to_many` (
    `customer_id` INT NOT NULL,
    `firstName` VARCHAR(45) NOT NULL,
    `lastName` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`customer_id`)
    ) ENGINE=InnoDB;


CREATE TABLE `order_one_to_many` (
    `order_id` INT NOT NULL,
    `description` VARCHAR(45) NOT NULL,
    `price` DOUBLE NOT NULL,
    `customer_id` INT NOT NULL,
    PRIMARY KEY (`order_id`),
    CONSTRAINT `customer_fk_one_to_many` FOREIGN KEY (`customer_id`) REFERENCES `customer_one_to_many` (`customer_id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB;

CREATE TABLE `order_many_to_many` (
    `order_id` INT NOT NULL,
    `description` VARCHAR(45) NOT NULL,
    `price` DOUBLE NOT NULL,
    PRIMARY KEY (`order_id`)
    ) ENGINE=InnoDB;

CREATE TABLE `item_many_to_many` (
    `item_id` INT NOT NULL,
    `description` VARCHAR(45) NOT NULL,
    `price` DOUBLE NOT NULL,
    PRIMARY KEY (`item_id`)
    ) ENGINE=InnoDB;

CREATE TABLE `order_item_many_to_many` (
    `order_item_id` INT NOT NULL,
    `order_id` INT NOT NULL,
    `item_id` INT NOT NULL,
    PRIMARY KEY (`order_item_id`),
    CONSTRAINT `order_fk` FOREIGN KEY (`order_id`) REFERENCES `order_many_to_many` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `item_fk` FOREIGN KEY (`item_id`) REFERENCES `item_many_to_many` (`item_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB;