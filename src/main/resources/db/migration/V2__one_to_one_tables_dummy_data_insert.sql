INSERT INTO customer_one_to_one (customer_id, firstName, lastName) VALUES (1, "John", "Smith");
INSERT INTO customer_one_to_one (customer_id, firstName, lastName) VALUES (2, "Dave", "Burn");
INSERT INTO customer_one_to_one (customer_id, firstName, lastName) VALUES (3, "Jane", "Walker");

INSERT INTO address_one_to_one (address_id, streetName, cityName, customer_id) VALUES (1, "Str Happy", "Boston", 1);
INSERT INTO address_one_to_one (address_id, streetName, cityName, customer_id) VALUES (2, "Str America", "Chicago", 2);
INSERT INTO address_one_to_one (address_id, streetName, cityName, customer_id) VALUES (3, "Str Apples", "New York", 3);



